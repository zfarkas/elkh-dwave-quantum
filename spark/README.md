# About

This directory contains the files necessary to build the Spark Master and Spark Worker images for the ELKH Cloud - DWave quantum demo.

Created by Zoltán Farkas <zfarkas@zfarkas.hu> based on the work of Gezim Sejdiu <g.sejdiu@gmail.com> and Giannis Mouchakis <gmouchakis@gmail.com> (see https://github.com/big-data-europe/docker-spark for details)
