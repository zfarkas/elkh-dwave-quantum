# Quantum reference architecture

This reference architecture provides a basis for experimenting with quantum resources. The RA is composed of the following set of services:
- [JupyterLab](https://jupyterlab.readthedocs.io/en/3.2.x/): the main entry point for using the RA, provides a development environment,
- [Apache Spark](https://spark.apache.org/): is a multi-language engine for executing data engineering, data science, and machine learning on single-node machines or clusters (please see the [Apache Spark RA of the ELKH Cloud](https://science-cloud.hu/en/reference-architectures/apache-spark-cluster-python-stack-jupyter-notebook-and-pyspark) for details).

This RA contains a number of pre-prepared examples to start learning quantum computing. Once started, the JupyterLab component can be used to investigate and run the provided samples.

# Prerequisites

Currently the RA can be deployed onto a Docker Swarm cluster. One can start such a cluster based on the [Docker Swarm RA for ELKH Cloud](https://science-cloud.hu/referencia-architekturak/docker-swarm-klaszter-kiepitese).

Beside Docker Swarm, the following software tools must be deployed:
- `git`: to clone this repository,
- `docker-compose`: to build and run the reference architecture.

## Quantum resources used

The examples currently provided are using the D-Wave Leap system, offering the possibility to experiment with an annealing quantum computer. In order to use these type of resources one must have a valid D-Wave Leap token, which can be acquired through the D-Wave Leap Dashboard. The Dashboard is available after registration [here](https://cloud.dwavesys.com/leap/), and looks like the following screenshot:

![leap_dashboard](docs/pics/leap_dashboard.png "Dashboard of D-Wave Leap")


If a user account is needed, then registration is available through [this link](https://cloud.dwavesys.com/leap/signup/). The registration requires the user to enter some information, as shown in the following screenshot:

![leap_signup](docs/pics/leap_signup.png "Signup screen of D-Wave Leap")


# Usage

The `docker-compose` tool is required to to use this RA:

```
$ git clone https://git.sztaki.hu/science-cloud/reference-architectures/quantum.git
$ cd quantum
$ docker-compose -f docker-compose.dev.yml build
```

At this point one may want to change the default password (`lpds`) for JupyterLab in the `docker-compose.dev.yml` file:
```
$ cat docker-compose.dev.yml
version: '3'
services:
...
  jupyterlab:
...
    environment:
      - PASSWORD=lpds
...
$ # update password if needed
$ docker-compose -f docker-compose.dev.yml up
```

Afterwards, the Jupyter Lab instance is accessible through [http://localhost:8888](http://localhost:8888) (please note: `localhost` should be changed to the IP address of your virtual machine when running the RA on the ELKH Cloud), using the password set in the compose file (default is `lpds`). The opening page looks as follows:

![jupyterlab_opening](docs/pics/jupyterlab_opening.png "Opening page of JupyterLab")

The left sidebar of JupyterLab serves as a filesystem browser, currently with the following examples deployed:
- `clustering` ([direct notebook link](http://localhost:8888/lab/tree/clustering/clustering.ipynb)): shows the implementation of the clustering problem using Apache Spark and the D-Wave quantum hardware.
- `evcharger` ([direct notebook link](http://localhost:8888/lab/tree/evcharger/evcharger.ipynb)): shows how to compute the "EV Charger Placement" problem on a virtual machine started in the ELKH Cloud using a simulator, and how to compute the same problem using D-Wave's binary quadratic model (BQM) hybrid solver, using an actual quantum hardware.
- `gates` ([and gate](http://localhost:8888/lab/tree/gates/and.ipynb) and [not gate](http://localhost:8888/lab/tree/gates/not.ipynb)): show how simple logical gate "problems" can be solved with D-Wave quantum hardware.
- `vertex-cover` ([direct notebook link](http://localhost:8888/lab/tree/vertex-cover/cover.ipynb)): shows the implementation of the vertex cover problem using a local simulator, and the D-Wave quantum hardware, on Erdős-Rényi graphs.

Note: all tools require that your D-Wave token is properly set. Please see the notebooks for this reminder.

The following screenshot shows the notebook of the `vertex-cover` example:

![vertex_cover](docs/pics/jupyterlab_vertex_cover.png "Notebook of the vertex-cover example")
