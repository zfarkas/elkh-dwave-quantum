import findspark
findspark.init()

from pyspark import SparkContext
from pyspark.mllib.clustering import KMeans
import matplotlib.pyplot as plt
import utilities


def cluster_points(scattered_points, image_filename):
    utilities.write_points_file(scattered_points, 'points.txt')
    sc = SparkContext(appName="KMeans", master="spark://spark-master:7077")
    sc.addFile('/notebook/clustering/utilities.py')
    lines = sc.textFile('points.txt')
    data = lines.map(utilities.parseVector)
    k = 3
    model = KMeans.train(data, k)
    print("Centers: " + str(model.clusterCenters))
    print("Total Cost: " + str(model.computeCost(data)))
    center_points = [(p[0], p[1]) for p in model.clusterCenters]
    clusters = utilities.get_clusters(data, model)
    plt.plot(*zip(*clusters[0]), "ro", *zip(*clusters[1]), "go", *zip(*clusters[2]), "bo", *zip(*center_points), "cD", markersize=12)
    plt.savefig(image_filename)