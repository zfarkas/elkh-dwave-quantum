import os
from notebook.auth import passwd

password = passwd(os.environ['PASSWORD'], algorithm='sha256')
print(str(password))
