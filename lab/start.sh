#!/bin/bash


jupyter server --generate-config

passwd=$(/usr/bin/python3 /usr/local/bin/setup_hashed_password.py)
echo "c.ServerApp.root_dir = '/notebook'" >> ~/.jupyter/jupyter_lab_config.py
echo "c.ServerApp.password = '$passwd'" >> ~/.jupyter/jupyter_lab_config.py

jupyter-lab --ip=0.0.0.0 --allow-root --no-browser --config=/root/.jupyter/jupyter_lab_config.py
